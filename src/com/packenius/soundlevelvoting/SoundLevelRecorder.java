package com.packenius.soundlevelvoting;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/**
 * Sound level measure recorder - give RMS and Peak to listener if this is set.
 * 
 * @author Christian Packenius, 2019.
 */
public class SoundLevelRecorder implements Runnable {
	private SoundLevelListener soundLevelListener = null;

	private final int sampleCount = 1024;
	private final int bufferByteSize = sampleCount * 2;

	byte[] buf = new byte[bufferByteSize];
	double[] samples = new double[sampleCount];

	double rms = 0f;
	double peak = 0f;

	@Override
	public void run() {
		TargetDataLine line = openAudioInputLine();

		double lastPeak = 0f;

		line.start();
		for (int b; (b = line.read(buf, 0, buf.length)) > -1;) {
			convertAudioBufferToSamples(buf, samples, b);

			// No listener? No event.
			if (soundLevelListener != null) {
				calculateRmsAndPeak();
				lastPeak = calculateLastPeak(lastPeak);
				soundLevelListener.setAmplitudeAndPeak(rms, peak);
			}
		}
	}

	private TargetDataLine openAudioInputLine() {
		AudioFormat fmt = new AudioFormat(44100f, 16, 1, true, false);
		TargetDataLine line = null;
		try {
			line = AudioSystem.getTargetDataLine(fmt);
			line.open(fmt, bufferByteSize);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			System.exit(-42);
		}
		return line;
	}

	private void convertAudioBufferToSamples(byte[] buf, double[] samples, int b) {
		for (int i = 0, s = 0; i < b;) {
			int sample = 0;

			// Little endian.
			sample |= buf[i++] & 0xFF;
			sample |= buf[i++] << 8;

			// In den Bereich [-1; 1] normalisieren.
			samples[s++] = sample / 32768f;
		}
	}

	private void calculateRmsAndPeak() {
		rms = 0f;
		peak = 0f;
		for (double sample : samples) {
			double abs = Math.abs(sample);
			if (abs > peak) {
				peak = abs;
			}
			rms += sample * sample;
		}
		rms = Math.sqrt(rms / samples.length);
	}

	private double calculateLastPeak(double lastPeak) {
		if (lastPeak > peak) {
			peak = lastPeak * 0.875f;
		}
		lastPeak = peak;
		return lastPeak;
	}

	public SoundLevelListener getSoundLevelListener() {
		return soundLevelListener;
	}

	public void setSoundLevelListener(SoundLevelListener soundLevelListener) {
		this.soundLevelListener = soundLevelListener;
	}
}
