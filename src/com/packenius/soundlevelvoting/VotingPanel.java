package com.packenius.soundlevelvoting;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * Panel for showing groups and voting results.
 * 
 * @author Christian Packenius, 2019.
 */
public class VotingPanel extends JPanel implements SoundLevelListener, KeyListener {
	private static final long serialVersionUID = -4592891840701978886L;

	/**
	 * Background color for group rectangles.
	 */
	private static final int bgValue = 0xffc0c020;
	private static final Color bgColor = new Color(bgValue, true);

	/**
	 * Last voted group (0..n-1).
	 */
	private int lastVotedGroupID = -1;

	/**
	 * Group data from properties.
	 */
	private int groupCount;
	private String[] groupNames;
	private BufferedImage[] groupIcons;

	/**
	 * Group data during/from votings.
	 */
	private double[] groupRMS;
	private double[] groupPeak;

	/**
	 * Currently voted group (0..n-1).
	 */
	private int currentVotedGroupID = -1;

	/**
	 * ID of winner group. Is set after last group voting.
	 */
	private int winnerID = -1;

	public VotingPanel(Properties props, SoundLevelRecorder slRecorder) {
		slRecorder.setSoundLevelListener(this);
		readGroupDataFromProperties(props);
	}

	private void readGroupDataFromProperties(Properties props) {
		groupCount = Integer.parseInt(props.getProperty("group-count"));
		groupNames = new String[groupCount];
		groupRMS = new double[groupCount];
		groupPeak = new double[groupCount];
		groupIcons = new BufferedImage[groupCount];
		for (int i = groupCount - 1; i >= 0; i--) {
			groupNames[i] = props.getProperty("group-name-" + (i + 1), "Gruppe " + (i + 1));
			try {
				groupIcons[i] = ImageIO.read(new File(props.getProperty("group-icon-file-" + (i + 1))));
			} catch (Exception e1) {
				// Use ghost as default image.
				try (InputStream in = VotingPanel.class.getResourceAsStream("ghost.png")) {
					groupIcons[i] = ImageIO.read(in);
				} catch (Exception e2) {
					// If ghost is absent, use four pixel colors.
					groupIcons[i] = new BufferedImage(2, 2, BufferedImage.TYPE_INT_ARGB);
					groupIcons[i].setRGB(0, 0, 0xffff0000);
					groupIcons[i].setRGB(1, 0, 0xff00ff00);
					groupIcons[i].setRGB(0, 1, 0xff0000ff);
					groupIcons[i].setRGB(1, 1, 0xffffff00);
				}
			}
			groupIcons[i] = normalizeGroupIcon(groupIcons[i]);
		}
	}

	/**
	 * Transformate group icon to circle and reformat to TYPE_INT_ARGB (if it is
	 * not).
	 */
	private BufferedImage normalizeGroupIcon(BufferedImage image) {
		int w = image.getWidth();
		int h = image.getHeight();

		BufferedImage i2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = i2.getGraphics();
		g.drawImage(image, 0, 0, w, h, null);
		g.dispose();
		image = i2;

		int mx = w / 2;
		int my = h / 2;
		int inside = Math.min(w, h) / 2;

		// Set all pixels outside of the inner circle to background color.
		for (int x = w - 1; x >= 0; x--) {
			for (int y = h - 1; y >= 0; y--) {
				int dx = x - mx;
				int dy = y - my;
				int delta = (int) Math.sqrt(dx * dx + dy * dy);
				if (delta > inside) {
					image.setRGB(x, y, bgValue);
				}
			}
		}

		return image;
	}

	@Override
	public void paint(Graphics gr) {
		Graphics2D g = (Graphics2D) gr;

		// Set antialiasing for text.
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		// Size of panel - background black.
		int width = getWidth();
		int height = getHeight();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);

		// Sizes for single group rectangle.
		int groupHeight = height / groupCount;
		int groupNameBorder = (int) (groupHeight * 0.1);
		int delta = groupNameBorder / 2;
		int iconSize = groupHeight * 2 / 3;

		// Draw groups from top to bottom.
		for (int groupID = 0; groupID < groupCount; groupID++) {
			// Get data of the current group.
			String groupName = groupNames[groupID];
			int groupY = groupHeight * groupID;
			double rms = groupRMS[groupID];
			double peak = groupPeak[groupID];

			// Draw group rectangle (rounded).
			g.setColor(bgColor);
			int rectWidth = width - 2 * delta;
			int rectHeight = groupHeight - 2 * delta;
			g.fillRoundRect(delta, groupY + delta, rectWidth, rectHeight, delta, delta);

			// Draw RMS (and peak of currently voted group).
			if (rms > 0) {
				g.setColor(Color.GREEN);

				// Set looser to gray.
				if (winnerID >= 0 && winnerID != groupID) {
					g.setColor(Color.GRAY);
				}
				g.fillRect(delta + groupHeight, groupY + delta, (int) (rectWidth * rms), rectHeight);

				// Set peak if this is the currently voted group.
				if (peak > 0) {
					g.setColor(Color.RED);
					g.fillRect(delta + groupHeight + (int) (rectWidth * peak), groupY + delta, 1, rectHeight);
				}
			}

			// Draw border of rectangle in dark gray.
			g.setColor(Color.DARK_GRAY);
			g.drawRoundRect(delta, groupY + delta, rectWidth, rectHeight, delta, delta);

			// Draw group logo.
			int iconX = (groupHeight - iconSize) / 2;
			int iconY = groupY + iconX;
			g.drawImage(groupIcons[groupID], iconX, iconY, iconSize, iconSize, null);

			// Size of font for group name?
			int maxNameHeight = groupHeight - 3 * groupNameBorder;
			int maxNameWidth = width - 2 * groupNameBorder - groupHeight; // Links das Group-Icon.
			Font font = new Font("Verdana", Font.BOLD, maxNameHeight);
			g.setFont(font);
			Rectangle2D bounds = g.getFontMetrics().getStringBounds(groupName, g);
			int nameWidth = (int) bounds.getWidth();

			// Use max size or smaller font?
			if (nameWidth > maxNameWidth) {
				// Smaller font - recalculate everything.
				maxNameHeight = maxNameHeight * maxNameWidth / nameWidth;
				font = new Font("Verdana", Font.BOLD, maxNameHeight);
				g.setFont(font);
				bounds = g.getFontMetrics().getStringBounds(groupName, g);
			}

			// Group text white - but blinking if winner.
			g.setColor(Color.WHITE);
			if (winnerID >= 0) {
				// Der Gewinner blinkt.
				if (winnerID == groupID) {
					int rr = (int) (Math.random() * 256);
					int gg = (int) (Math.random() * 256);
					int bb = (int) (Math.random() * 256);
					Color col = new Color(rr, gg, bb);
					g.setColor(col);
				}
			}

			// Draw name right aligned.
			double groupNameX = width - groupNameBorder - delta - bounds.getWidth();
			double groupNameY = groupY + (groupHeight - bounds.getHeight()) / 2 - bounds.getY();
			g.drawString(groupName, (int) groupNameX, (int) groupNameY);
		}

		repaint();
	}

	@Override
	public void setAmplitudeAndPeak(double rms, double peak) {
		if (currentVotedGroupID >= 0) {
			groupRMS[currentVotedGroupID] = Math.max(rms, groupRMS[currentVotedGroupID]);
			groupPeak[currentVotedGroupID] = peak;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Ignore.
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// Ignore.
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyChar()) {
		case ' ':
			if (winnerID >= 0) {
				break;
			}

			if (currentVotedGroupID < 0) {
				lastVotedGroupID++;
				if (lastVotedGroupID >= groupCount) {
					lastVotedGroupID = 0;
				}
				currentVotedGroupID = lastVotedGroupID;

				// Init RMS.
				groupRMS[currentVotedGroupID] = 0;
				groupPeak[currentVotedGroupID] = 0;
			} else {
				// Stop current voting.
				groupPeak[currentVotedGroupID] = 0;
				currentVotedGroupID = -1;

				// If this has been the last group, get winner ID.
				if (lastVotedGroupID + 1 == groupCount) {
					double rms = 0;
					for (int i = groupCount - 1; i >= 0; i--) {
						if (rms < groupRMS[i]) {
							rms = groupRMS[i];
							winnerID = i;
						}
					}
				}
			}
			break;
		}
	}
}
