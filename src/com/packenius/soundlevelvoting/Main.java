package com.packenius.soundlevelvoting;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.JFrame;

public class Main {
	public static void main(String[] args) throws IOException {
		Properties props = new Properties();
		try (InputStream in = new FileInputStream("group-data/groups.properties")) {
			props.load(in);
		}

		JFrame frame = new JFrame();
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		SoundLevelRecorder slRecorder = new SoundLevelRecorder();
		new Thread(slRecorder).start();

		VotingPanel content = new VotingPanel(props, slRecorder);

		content.addKeyListener(content);
		frame.addKeyListener(content);

		frame.setContentPane(content);
		frame.setVisible(true);
	}
}
