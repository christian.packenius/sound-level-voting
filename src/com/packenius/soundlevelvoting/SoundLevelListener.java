package com.packenius.soundlevelvoting;

public interface SoundLevelListener {
	void setAmplitudeAndPeak(double rms, double peak);
}