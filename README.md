# sound-level-voting

GUI for voting some groups/persons/instances with loudness, for example clapping or stamping.

# Encoding

Java source encoding is UTF-8.

# How to use

1. Open group-data/groups.properties.
2. Set number of group in field "group-count".
3. Set group names in fields "group-name-*", using numbers 1, 2, ... for the star.
4. Set name of group logos in fields "group-icon-file-*".

Groups without given name will get name "Gruppe *".

Groups without given logo will have a PacMan ghost icon as their logo.

# Links for measuring peak and RMS

https://stackoverflow.com/questions/26574326/how-to-calculate-the-level-amplitude-db-of-audio-signal-in-java
https://stackoverflow.com/questions/26824663/how-do-i-use-audio-sample-data-from-java-sound

(Interesting for understanding algorithm of measuring audio signal.)

